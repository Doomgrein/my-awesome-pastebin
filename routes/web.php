<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'PasteMiddleware'], function () {
    Route::get('/', 'IndexController@index');

    Route::post('/', 'PasteController@pasteSave')->name('pasteSave');
    Route::get('paste/{hash}', 'PasteController@index')->name('pasteShow');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('login/vkontakte', 'Auth\LoginController@redirectToProvider');
Route::get('login/vkontakte/callback', 'Auth\LoginController@handleProviderCallback');