@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">My Awesome Pastebin</div>

                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Вы авторизированы!

                    <br>
                    <a href="/"> Перейти к главной странице </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
