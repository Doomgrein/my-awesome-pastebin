@extends ('layouts.layout')

@section ('content')

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <h3> Выбранная тема: <i>{{ $paste->title }}</i></h3>
        <p> Текст темы: <i>{{ $paste->text }}</i></p>
        <a class="btn btn-primary" href="/" role="button"> Вернуться на главную страницу </a>
    </div>

@endsection