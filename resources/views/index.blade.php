@extends ('layouts.layout')

@section ('content')

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        {{ Form::open(array('route' => ['pasteSave'])) }}

            <div class="form-group">
                <label> Новая паста! </label>
                <div class="form-group">
                    <input type="hidden" name="link" value="{{ hash('adler32', str_random(8)) }}">
                    <input type="text" class="form-control" id="pasteTheme" placeholder="Введите тему" name="title">
                </div>
                <textarea class="form-control" id="pasteText" rows="5" placeholder="Введите текст" name="text"></textarea>
                <br>
                <label for="expirationTime"> Время доступа к пасте: </label>
                <select class="form-control" id="expirationTime" name="timestamp">
                    <option value="{{ $time = time() + 60 }}">                 1 минута</option>
                    <option value="{{ $time = time() +(10 * 60) }}">           10 минут</option>
                    <option value="{{ $time = time() +(60 * 60) }}">           1 час</option>
                    <option value="{{ $time = time() +(3 * 60 * 60) }}">       3 часа</option>
                    <option value="{{ $time = time() +(24 * 60 * 60) }}">      1 день</option>
                    <option value="{{ $time = time() +(7 * 24 * 60 * 60) }}">  1 неделя</option>
                    <option value="{{ $time = time() +(31 * 24 * 60 * 60) }}"> 1 месяц</option>
                    <option value="{{ $time = 0 }}">                           Без ограничений</option>
                </select>
                <br>
                <label for="status"> Статус: </label>
                <select class="form-control" id="status" name="status">
                    <option value="public">   Публичный </option>
                    <option value="unlisted"> Скрытый   </option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary"> Сохранить </button>

        {{ Form::close() }}
    </div>

@endsection