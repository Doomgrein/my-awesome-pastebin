@extends ('layouts.layout')

@section ('content')

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div>
            <p>Ваша ссылка на пасту: <a href="{{ route('pasteShow', $paste->link) }}">http://my-awesome-pastebin.tld/paste/{{ $paste->link }}</a></p>
            <br>
            <a class="btn btn-primary" href="/" role="button"> Вернуться на главную страницу </a>
        </div>
    </div>

@endsection