<?php

namespace App\Http\Middleware;

use App\Models\Paste;
use Closure;

class PasteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Проверка времени доступа пасты (Expiration Time)
         */
        $pastes = Paste::all();
        foreach ($pastes as $paste)
        {
            $currentTime = time();
            $limitTime = $paste->timestamp;
            if ($limitTime == 0)
            {
                continue;
            }
            if ($currentTime >= $limitTime)
            {
                if ($limitTime == 0)
                {
                    continue;
                } else {
                    Paste::find($paste->id)->delete();
                }
            }
        }

        return $next($request);
    }
}
