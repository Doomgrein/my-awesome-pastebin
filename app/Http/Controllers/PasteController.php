<?php

namespace App\Http\Controllers;

use App\Models\Paste;
use App\Services\PasteService;
use Illuminate\Http\Request;

class PasteController extends Controller
{

    /**
     * @param $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($hash)
    {
        $pastes = Paste::where('status', '=', 'public')->paginate(10);
        $paste = Paste::where('link', '=', $hash)->first();
        return view ('paste')->with([
            'paste' => $paste,
            'pastes' => $pastes
        ]);
    }

    /**
     * @param Request $request
     * @param PasteService $pasteService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pasteSave(Request $request, PasteService $pasteService)
    {
        $pastes = Paste::where('status', '=', 'public')->paginate(10);
        $pasteService->create($request->all());
        $data = $request->all();
        $link = array_get($data, 'link');
        $paste = Paste::where('link', '=', $link)->first();
        return view('link', [
            'pastes' => $pastes,
            'paste' => $paste
        ]);
    }
}
