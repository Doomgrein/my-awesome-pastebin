<?php

namespace App\Http\Controllers;

use App\Models\Paste;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pastes = Paste::where('status', '=', 'public')->paginate(10);
        return view ('index', ['pastes' => $pastes]);
    }

}
