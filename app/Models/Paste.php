<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paste extends Model
{
    protected $fillable = ['title', 'text', 'id', 'timestamp', 'status', 'link'];
}
