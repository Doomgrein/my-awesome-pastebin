<?php
/**
 * Created by PhpStorm.
 * User: Doomgrein
 * Date: 09.01.2019
 * Time: 1:04
 */
namespace App\Services;

use App\Models\Paste;

class PasteService
{
    /**
     * @param array $fields
     * @return Paste
     */
    public function create(array $fields)
    {
        $paste = new Paste;
        $paste->fill($fields);
        $paste->save();
        return $paste;
    }

}
